<?php
/*
Plugin Name: Restrict Webcast Page
Version: 1.2
Description: Restricts non authenticated users to access webcasts pages.
Author: Elvis Morales
Author URI: https://twitter.com/n3rdh4ck3r
Plugin URI: https://bitbucket.org/grantcardone/restrict-webcast-page
Text Domain: restrict-webcast-page
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


// Redirect to home page if user is trying to reach webcast video page and is not logged in.
add_action( 'template_redirect', function() {
	global $wp, $webcast_link;
	if ( home_url( $wp->request ) == substr( $webcast_link, 0, -1 ) && !is_user_logged_in() ) { 
		wp_redirect( home_url() );
		exit(); 
	}
} );
